export JAVA_HOME={{ __openjdk_install_dir }}/jdk-{{ openjdk_version }}
export PATH=$PATH:$JAVA_HOME/bin
